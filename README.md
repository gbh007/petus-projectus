# Petus-projectus

[GitHub](https://github.com/gbh007/petus-projectus)/[Gitlab](https://gitlab.com/gbh007/petus-projectus)

> _История о том как сделать нажатие кнопки через микросервисы_
>
> _Сколько микросервисов нужно чтобы нажать кнопку_
>
> _Нет ничего проще чем сделать микросервис нажатия на кнопку_
>
> _Юай на гошке не бесполезен, у него просто особое применение_
>
> _Код не очень, но это же мвп, чудо что запустилось_

## Запуск

Достаточно запустить команду, или выполнить ее внутренние команды

> make up

## TODO

1. Рефакторинг работы с ошибками
   - Привести в один вид, предварительно в `PACKAGE_NAME: FUNC_NAME: ERR`
2. Рефакторинг логов пользователя (после реализации внутреннего функционала):
   - Необходимо добавить новые поля в сигнатуру через кафку
   - Необходимо добавить новые поля в БД
3. Переход от экспорта в Jaeger на OtelCollector

## Jaeger

> `http://localhost:16686/search`

## Примеры запросов для prometheus

Панель для метрик доступна по адресу - <http://localhost:19090/consoles/custom/simple.html>

Для графаны доступен дашборд - `grafana.json`

Среднее время бизнес обработки задачи

```plain
  sum by (instance) (rate(petus_projectus_worker_business_handle_time_sum[1m]))
/
  sum by (instance) (rate(petus_projectus_worker_business_handle_time_count[1m]))
```

Размер очереди задач

```plain
sum(petus_projectus_handler_handle_time_count) - sum(petus_projectus_worker_handle_time_count)
```

Среднее время выполнения запроса

```plain
  sum by (instance) (rate(petus_projectus_protobuf_request_duration_sum[1m]))
/
  sum by (instance) (rate(petus_projectus_protobuf_request_duration_count[1m]))
```

Количество запросов в секунду

```plain
sum by (instance) (rate(petus_projectus_protobuf_request_duration_count[1m]))
```

## Общая схема

![schema](scheme.drawio.png)
